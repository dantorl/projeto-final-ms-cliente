FROM openjdk:8-jre-alpine
COPY target/cliente*.jar /app.jar
EXPOSE 8080
ARG mysql_host
ARG mysql_port
ARG mysql_user
ARG mysql_pass
ENV mysql_host=$mysql_host mysql_port=$mysql_port mysql_user=$mysql_user mysql_pass=$mysql_pass
CMD ["java", "-Dserver.port=8080", "-jar", "app.jar"] 

