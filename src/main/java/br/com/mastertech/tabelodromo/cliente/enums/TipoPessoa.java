package br.com.mastertech.tabelodromo.cliente.enums;

public enum TipoPessoa {
    PF, PJ;
}
