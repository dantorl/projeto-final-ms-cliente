package br.com.mastertech.tabelodromo.cliente.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cliente já existe")
public class ClienteAlreadyExistsException extends RuntimeException{
}
