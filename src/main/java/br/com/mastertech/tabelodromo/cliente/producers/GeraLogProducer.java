package br.com.mastertech.tabelodromo.cliente.producers;

import br.com.mastertech.producer.Log;
import br.com.mastertech.tabelodromo.cliente.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class GeraLogProducer {
    @Autowired
    private KafkaTemplate<String, Log> producer;

    @Value("${spring.profiles.active}")
    private String env;

    public void geraLogInclusaoCliente(Cliente cliente) {
        Log registroLog = new Log();

        Timestamp dataDeHoje = new Timestamp(System.currentTimeMillis());
        registroLog.setDateTime(dataDeHoje);

        registroLog.setEnv(env);
        registroLog.setService("Cliente");
        registroLog.setClassExec("ClienteService");
        registroLog.setMethod("POST");
        registroLog.setKey(cliente.getIdentidade());
        registroLog.setLog("Cliente gerado com sucesso!");

        producer.send("spec2-logs", registroLog);

    }
    public void geraLogInclusaoClienteError(Cliente cliente) {
        Log registroLog = new Log();

        Timestamp dataDeHoje = new Timestamp(System.currentTimeMillis());
        registroLog.setDateTime(dataDeHoje);

        registroLog.setEnv(env);
        registroLog.setService("Cliente");
        registroLog.setClassExec("ClienteService");
        registroLog.setMethod("POST");
        registroLog.setKey(cliente.getIdentidade());
        registroLog.setLog("Cliente já existe!");

        producer.send("spec2-logs", registroLog);
    }

    public void geraLogAtualizacaoCliente(Cliente cliente) {
        Log registroLog = new Log();

        Timestamp dataDeHoje = new Timestamp(System.currentTimeMillis());
        registroLog.setDateTime(dataDeHoje);

        registroLog.setEnv(env);
        registroLog.setService("Cliente");
        registroLog.setClassExec("ClienteService");
        registroLog.setMethod("PUT");
        registroLog.setKey(cliente.getIdentidade());
        registroLog.setLog("Cliente atualizado com sucesso!");

        producer.send("spec2-logs", registroLog);

    }
    public void geraLogConsultaClientePorID(Cliente cliente) {
        Log registroLog = new Log();

        Timestamp dataDeHoje = new Timestamp(System.currentTimeMillis());
        registroLog.setDateTime(dataDeHoje);

        registroLog.setEnv(env);
        registroLog.setService("Cliente");
        registroLog.setClassExec("ClienteService");
        registroLog.setMethod("GET");
        registroLog.setKey(cliente.getIdentidade());
        registroLog.setLog("Cliente consultado com sucesso pelo ID " + cliente.getId() + ".");

        producer.send("spec2-logs", registroLog);

    }
    public void geraLogConsultaClientePorIDError(Integer id) {
        Log registroLog = new Log();

        Timestamp dataDeHoje = new Timestamp(System.currentTimeMillis());
        registroLog.setDateTime(dataDeHoje);

        registroLog.setEnv(env);
        registroLog.setService("Cliente");
        registroLog.setClassExec("ClienteService");
        registroLog.setMethod("GET");
        registroLog.setKey(" ");
        registroLog.setLog("Cliente não encontrato pelo ID " + id + ".");

        producer.send("spec2-logs", registroLog);
    }
    public void geraLogConsultaTodosClientes() {
        Log registroLog = new Log();
        Timestamp dataDeHoje = new Timestamp(System.currentTimeMillis());
        registroLog.setDateTime(dataDeHoje);

        registroLog.setEnv(env);
        registroLog.setService("Cliente");
        registroLog.setClassExec("ClienteService");
        registroLog.setMethod("GET");
        registroLog.setKey(" ");
        registroLog.setLog("Solicitada busca de todos clientes!");

        producer.send("spec2-logs", registroLog);
    }

    public void geraLogConsultaClientePorIdentidade(Cliente cliente) {
        Log registroLog = new Log();

        Timestamp dataDeHoje = new Timestamp(System.currentTimeMillis());
        registroLog.setDateTime(dataDeHoje);

        registroLog.setEnv(env);
        registroLog.setService("Cliente");
        registroLog.setClassExec("ClienteService");
        registroLog.setMethod("GET");
        registroLog.setKey(cliente.getIdentidade());
        registroLog.setLog("Cliente consultado com sucesso pelo CPF/CNPJ " + cliente.getIdentidade() + ".");

        producer.send("spec2-logs", registroLog);

    }
    public void geraLogConsultaClientePorIdentidadeError(String identidade) {
        Log registroLog = new Log();

        Timestamp dataDeHoje = new Timestamp(System.currentTimeMillis());
        registroLog.setDateTime(dataDeHoje);

        registroLog.setEnv(env);
        registroLog.setService("Cliente");
        registroLog.setClassExec("ClienteService");
        registroLog.setMethod("GET");
        registroLog.setKey(" ");
        registroLog.setLog("Cliente não encontrato pelo CPF/CNPJ " + identidade + ".");

        producer.send("spec2-logs", registroLog);
    }
}
