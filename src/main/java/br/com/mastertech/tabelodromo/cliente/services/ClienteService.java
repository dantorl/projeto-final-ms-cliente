package br.com.mastertech.tabelodromo.cliente.services;

import br.com.mastertech.tabelodromo.cliente.producers.GeraLogProducer;
import br.com.mastertech.tabelodromo.cliente.exceptions.ClienteAlreadyExistsException;
import br.com.mastertech.tabelodromo.cliente.models.Cliente;
import br.com.mastertech.tabelodromo.cliente.repositories.ClienteRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    GeraLogProducer geraLogProducer;

    public Cliente cadastrarCliente(Cliente cliente) {
        Optional<Cliente> clienteOptional = clienteRepository.findByIdentidade(cliente.getIdentidade());
        if (clienteOptional.isPresent()) {
            geraLogProducer.geraLogInclusaoClienteError(cliente);
            throw new ClienteAlreadyExistsException();
        } else {
            Cliente clienteObjeto = clienteRepository.save(cliente);
            geraLogProducer.geraLogInclusaoCliente(clienteObjeto);
            return clienteObjeto;
        }
    }

    public Iterable<Cliente> buscarTodosClientes() {
        Iterable<Cliente> clientes = clienteRepository.findAll();
        geraLogProducer.geraLogConsultaTodosClientes();
        return clientes;
    }

    public Optional<Cliente> buscarClientePorId(Integer id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if (clienteOptional.isPresent()) {
            geraLogProducer.geraLogConsultaClientePorID(clienteOptional.get());
            return clienteOptional;
        } else {
            geraLogProducer.geraLogConsultaClientePorIDError(id);
            throw new ObjectNotFoundException("","Nao localizado Cliente com o Código informado");
        }
    }

    public Cliente atualizarClientePorId(Cliente cliente) {
        Optional<Cliente> clienteOptional = buscarClientePorId(cliente.getId());
        if (clienteOptional.isPresent()){
            Cliente clienteData = clienteOptional.get();
            if(cliente.getName() == null){
                cliente.setName(clienteData.getName());
            }
            if(cliente.getIdentidade() == null){
                cliente.setIdentidade(clienteData.getIdentidade());
            }
            if(cliente.getRenda() == null){
                cliente.setRenda(clienteData.getRenda());
            }
            if(cliente.getTipoPessoa() == null){
                cliente.setTipoPessoa(clienteData.getTipoPessoa());
            }
        }
        Cliente clienteObj = clienteRepository.save(cliente);
        geraLogProducer.geraLogAtualizacaoCliente(clienteObj);
        return clienteObj;
    }

    public Optional<Cliente> buscarClientePorIdentidade(String identidade) {
        Optional<Cliente> clienteOptional = clienteRepository.findByIdentidade(identidade);
        if (clienteOptional.isPresent()) {
            geraLogProducer.geraLogConsultaClientePorIdentidade(clienteOptional.get());
            return clienteOptional;
        } else {
            geraLogProducer.geraLogConsultaClientePorIdentidadeError(identidade);
            throw new ObjectNotFoundException("","Nao localizado Cliente com CPF/CNPJ informado");
        }
    }
}
