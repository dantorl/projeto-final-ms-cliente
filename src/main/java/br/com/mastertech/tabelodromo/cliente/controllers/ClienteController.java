package br.com.mastertech.tabelodromo.cliente.controllers;

import br.com.mastertech.tabelodromo.cliente.services.ClienteService;
import br.com.mastertech.tabelodromo.cliente.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> cadastrarCliente(@RequestBody @Valid Cliente cliente) {

        Cliente clienteObjeto = clienteService.cadastrarCliente(cliente);
        return ResponseEntity.status(201).body(clienteObjeto);
    }

    @GetMapping
    public Iterable<Cliente> buscarTodosClientes() {
        return clienteService.buscarTodosClientes();
    }

    @GetMapping("/{id}")
    public Cliente buscarClientePorId(@PathVariable Integer id) {
        Optional<Cliente> clienteOptional;
        try{
            clienteOptional = clienteService.buscarClientePorId(id);
            return clienteOptional.get();
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
    }

    @PutMapping("/{id}")
    public Cliente atualizarClientePorId(@PathVariable Integer id, @RequestBody Cliente cliente) {
        cliente.setId(id);
        Cliente clienteObj = clienteService.atualizarClientePorId(cliente);
        return clienteObj;
    }
    @GetMapping("/identidade/{identidade}")
    public Cliente buscarClientePorIdentidade(@PathVariable String identidade) {
        Optional<Cliente> clienteOptional;
        try{
            clienteOptional = clienteService.buscarClientePorIdentidade(identidade);
            return clienteOptional.get();
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
    }
}
