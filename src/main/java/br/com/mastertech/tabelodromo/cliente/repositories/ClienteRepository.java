package br.com.mastertech.tabelodromo.cliente.repositories;

import br.com.mastertech.tabelodromo.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
    Optional<Cliente> findByIdentidade(String identidade);
}
