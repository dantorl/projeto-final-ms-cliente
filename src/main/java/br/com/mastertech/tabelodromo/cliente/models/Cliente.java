package br.com.mastertech.tabelodromo.cliente.models;

import br.com.mastertech.tabelodromo.cliente.enums.TipoPessoa;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Table(name = "Cliente")
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(min = 4, max = 100, message = "O nome deve ter no minimo 4 caracteres e no maximo 100")
    @NotNull
    private String name;

    private TipoPessoa tipoPessoa;

    private String identidade;

    private BigDecimal renda;

    public Cliente() {
    }

    public Cliente(Integer id, @Size(min = 4, max = 100, message = "O nome deve ter no minimo 4 caracteres e no maximo 100") @NotNull String name, TipoPessoa tipoPessoa, String identidade, BigDecimal renda) {
        this.id = id;
        this.name = name;
        this.tipoPessoa = tipoPessoa;
        this.identidade = identidade;
        this.renda = renda;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public BigDecimal getRenda() {
        return renda;
    }

    public void setRenda(BigDecimal renda) {
        this.renda = renda;
    }
}
