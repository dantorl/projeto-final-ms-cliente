package br.com.mastertech.tabelodromo.cliente.models;

import java.sql.Timestamp;

public class RegistroLogCliente {
    private Timestamp dateTime;
    private String envName;
    private String serviceName;
    private String className;
    private String method;
    private String key;
    private String log;

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public String getEnvName() {
        return envName;
    }

    public void setEnvName(String envName) {
        this.envName = envName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public RegistroLogCliente() {
    }

    public RegistroLogCliente(Timestamp dateTime, String envName, String serviceName, String className, String method, String key, String log) {
        this.dateTime = dateTime;
        this.envName = envName;
        this.serviceName = serviceName;
        this.className = className;
        this.method = method;
        this.key = key;
        this.log = log;
    }
}
