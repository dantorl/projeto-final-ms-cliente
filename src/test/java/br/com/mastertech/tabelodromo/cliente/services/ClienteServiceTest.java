package br.com.mastertech.tabelodromo.cliente.services;

import br.com.mastertech.tabelodromo.cliente.enums.TipoPessoa;
import br.com.mastertech.tabelodromo.cliente.exceptions.ClienteAlreadyExistsException;
import br.com.mastertech.tabelodromo.cliente.models.Cliente;
import br.com.mastertech.tabelodromo.cliente.repositories.ClienteRepository;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

@SpringBootTest
public class ClienteServiceTest {
    @MockBean
    ClienteRepository clienteRepository;    @Autowired
    ClienteService clienteService;    Cliente cliente;    @BeforeEach
    public void inicializar(){
        Calendar calendar = new GregorianCalendar();
        calendar = new GregorianCalendar(2020, Calendar.JULY, 18);        cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Carolina Silva Maia");
        cliente.setIdentidade("381.428.528-02");
        cliente.setRenda(new BigDecimal(300));
        cliente.setTipoPessoa(TipoPessoa.PF);
    }    @Test
    public void testarSalvarClienteSucesso(){
        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).thenReturn(cliente);        Cliente clienteObjeto = clienteService.cadastrarCliente(cliente);        Assertions.assertEquals(cliente, clienteObjeto);
        Assertions.assertEquals(cliente.getName(), clienteObjeto.getName());
    }    @Test
    public void testarSalvarClienteJaExiste(){
        Mockito.when(clienteRepository.findByIdentidade(Mockito.any(String.class))).thenReturn(Optional.of(cliente));        Assertions.assertThrows(ClienteAlreadyExistsException.class,() ->{clienteService.cadastrarCliente(cliente);});
    }    @Test
    public void testarBuscarTodosClientesSucesso(){
        Iterable<Cliente> clienteIterable = Arrays.asList(cliente);        Mockito.when(clienteRepository.findAll()).thenReturn(clienteIterable);
        Iterable<Cliente> iterableResultado = clienteService.buscarTodosClientes();
        Assertions.assertEquals(clienteIterable, iterableResultado);
    }    @Test
    public void testarBuscarTodosClientesVazio(){
        Iterable<Cliente> clienteIterable = Arrays.asList(new Cliente());        Mockito.when(clienteRepository.findAll()).thenReturn(clienteIterable);
        Iterable<Cliente> iterableResultado = clienteService.buscarTodosClientes();
        Assertions.assertEquals(clienteIterable, iterableResultado);
    }    @Test
    public void testarBuscarPorIdSucesso(){
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(cliente));
        int id = 1;
        Optional clienteOptional = clienteService.buscarClientePorId(id);
        Assertions.assertEquals(cliente, clienteOptional.get());
    }    @Test
    public void testarBuscarPorIdInexistente(){
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        int id = 1;
        Assertions.assertThrows(ObjectNotFoundException.class,() ->{clienteService.buscarClientePorId(id);});
    }    @Test
    public void testarAtualizarClienteSucesso(){
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(cliente));
        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).thenReturn(cliente);        Cliente clienteObjeto = clienteService.atualizarClientePorId(cliente);
        Assertions.assertEquals(cliente, clienteObjeto);
    }    @Test
    public void testarAtualizarClienteInexistente(){
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).thenReturn(null);        Assertions.assertThrows(ObjectNotFoundException.class,() ->{clienteService.atualizarClientePorId(cliente);});
    }    @Test
    public void testarAtualizarClienteComNomeNuloSucesso(){
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(cliente));
        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).thenReturn(cliente);
        cliente.setName(null);
        Cliente clienteObjeto = clienteService.atualizarClientePorId(cliente);
        Assertions.assertEquals(cliente, clienteObjeto);
    }    @Test
    public void testarAtualizarClienteComIdentidadeNuloSucesso(){
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(cliente));
        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).thenReturn(cliente);
        cliente.setIdentidade(null);
        Cliente clienteObjeto = clienteService.atualizarClientePorId(cliente);
        Assertions.assertEquals(cliente, clienteObjeto);
    }    @Test
    public void testarAtualizarClienteComRendaNulaSucesso(){
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(cliente));
        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).thenReturn(cliente);
        cliente.setRenda(null);
        Cliente clienteObjeto = clienteService.atualizarClientePorId(cliente);
        Assertions.assertEquals(cliente, clienteObjeto);
    }    @Test
    public void testarAtualizarClienteComTipoPessoaNuloSucesso(){
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(cliente));
        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).thenReturn(cliente);
        cliente.setTipoPessoa(null);
        Cliente clienteObjeto = clienteService.atualizarClientePorId(cliente);
        Assertions.assertEquals(cliente, clienteObjeto);
    }
}
