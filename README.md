# Projeto-Final-MS-Cliente

Microsserviço de cliente do projeto final Tabelodromo

**Métodos:**

POST - 18.222.196.215/cliente
{
"name": "Nome Completo",
"tipoPessoa": "PF",
"identidade": "12314201828",
"renda": 1000.00
}

GET - 18.222.196.215/cliente
GET - 18.222.196.215/cliente/{id}
GET - 18.222.196.215/cliente/identidade/{cpf_cnpj}

PUT - 18.222.196.215/cliente/{id}
{
"name": "Nome Completo",
"tipoPessoa": "PF",
"identidade": "12314201828",
"renda": 1000.00
}

**Tópico kafka:** spec2-logs

**Model Cliente:**

private Integer id;
private String name;
private TipoPessoa tipoPessoa;
private String identidade;
private BigDecimal renda;